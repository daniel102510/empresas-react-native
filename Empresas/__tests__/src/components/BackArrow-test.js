/**
 * @format
 */

import 'react-native';
import React from 'react';
import BackArrow from '../../../src/components/BackArrow';
import {Provider} from 'react-redux';

import store from '../../../__mock__/storeMock';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const component = renderer.create(
    <Provider store={store}>
      <BackArrow />
    </Provider>,
  );
  expect(component.toJSON()).toMatchSnapshot();
});
