import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../src/states/sagas';
import allReducers from '../src/states/ducks';

const sagaMiddleware = createSagaMiddleware({});

const store = createStore(
  allReducers,
  compose(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

export default store;
