import React, {useEffect} from 'react';
import {View, Text, ScrollView, Image, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchEnterprise as fetchEnterpriseAction} from '../../states/ducks/enterprises';

// Components
import Loader from '../../components/Loader';
import BackArrow from '../../components/BackArrow';

// Styles
import Styles from './styles';
import constants from '../../utils/constants';

const Home = (props) => {
  const {loading, enterprises, navigation, actions, route} = props;

  useEffect(() => {
    actions.fetchEnterprise(_.get(route, 'params.id'));
  }, []);

  return (
    <SafeAreaView>
      {!loading.isLogged &&
      _.get(enterprises, 'enterpraiseDetail.photo') !== undefined ? (
        <View style={Styles.container}>
          <BackArrow
            title="Enterprise details"
            onPress={() => navigation.goBack()}
          />
          <ScrollView styles={Styles.scrollview}>
            <Image
              style={Styles.image}
              source={{
                uri: `${constants.endpoint}${_.get(
                  enterprises,
                  'enterpraiseDetail.photo',
                )}`,
              }}
            />
            <View style={Styles.scrollviewContent}>
              <View style={Styles.viewTitle}>
                <Text style={Styles.name}>
                  {_.get(enterprises, 'enterpraiseDetail.enterprise_name')}
                </Text>
                <View style={Styles.label}>
                  <View style={Styles.viewType}>
                    <Text style={Styles.textType}>
                      {_.get(
                        enterprises,
                        'enterpraiseDetail.enterprise_type.enterprise_type_name',
                      )}
                    </Text>
                  </View>

                  <Text style={Styles.textCity}>
                    {_.get(enterprises, 'enterpraiseDetail.city')} /{' '}
                    {_.get(enterprises, 'enterpraiseDetail.country')}
                  </Text>
                </View>
              </View>

              <Text style={Styles.description}>
                {_.get(enterprises, 'enterpraiseDetail.description')}
              </Text>
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.email_enterprise'),
              ) && (
                <View style={Styles.viewIcon}>
                  <Icon name="envelope" size={15} color="rgba(0,0,0,0.7)" />
                  <Text style={Styles.textIcon}>
                    {_.get(enterprises, 'enterpraiseDetail.email_enterprise')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.facebook'),
              ) && (
                <View style={Styles.viewIcon}>
                  <Icon name="facebook" size={15} color="rgba(0,0,0,0.7)" />
                  <Text style={Styles.textIcon}>
                    {_.get(enterprises, 'enterpraiseDetail.facebook')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.twitter'),
              ) && (
                <View style={Styles.viewIcon}>
                  <Icon name="twitter" size={15} color="rgba(0,0,0,0.7)" />
                  <Text style={Styles.textIcon}>
                    {_.get(enterprises, 'enterpraiseDetail.twitter')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.linkedin'),
              ) && (
                <View style={Styles.viewIcon}>
                  <Icon name="linkedin" size={15} color="rgba(0,0,0,0.7)" />
                  <Text style={Styles.textIcon}>
                    {_.get(enterprises, 'enterpraiseDetail.linkedin')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.phone'),
              ) && (
                <View style={Styles.viewIcon}>
                  <Icon name="phone" size={15} color="rgba(0,0,0,0.7)" />
                  <Text style={Styles.textIcon}>
                    {_.get(enterprises, 'enterpraiseDetail.phone')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.value'),
              ) && (
                <View style={Styles.viewRow}>
                  <Text style={Styles.titleRow}>Value</Text>
                  <Text style={Styles.subTitleRow}>
                    $ {_.get(enterprises, 'enterpraiseDetail.value')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.shares'),
              ) && (
                <View style={Styles.viewRow}>
                  <Text style={Styles.titleRow}>Shares</Text>
                  <Text style={Styles.subTitleRow}>
                    {_.get(enterprises, 'enterpraiseDetail.shares')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.share_price'),
              ) && (
                <View style={Styles.viewRow}>
                  <Text style={Styles.titleRow}>Share Price</Text>
                  <Text style={Styles.subTitleRow}>
                    $ {_.get(enterprises, 'enterpraiseDetail.share_price')}
                  </Text>
                </View>
              )}
              {!['', null].includes(
                _.get(enterprises, 'enterpraiseDetail.own_shares'),
              ) && (
                <View style={Styles.viewRow}>
                  <Text style={Styles.titleRow}>Own Shares</Text>
                  <Text style={Styles.subTitleRow}>
                    {_.get(enterprises, 'enterpraiseDetail.own_shares')}
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      ) : (
        <View style={Styles.container}>
          <Loader />
        </View>
      )}
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  const {loadingReducer, userReducer, enterprisesReducer} = state;

  return {
    loading: loadingReducer,
    user: userReducer,
    enterprises: enterprisesReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      fetchEnterprise: bindActionCreators(fetchEnterpriseAction, dispatch),
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
