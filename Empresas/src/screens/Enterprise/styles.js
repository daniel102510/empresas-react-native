import {StyleSheet, Platform} from 'react-native';
import constants from '../../utils/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: constants.primaryLightColor,
  },
  scrollviewContent: {
    width: '100%',
    paddingHorizontal: '5%',
    backgroundColor: constants.primaryLightColor,
    top: -20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  image: {width: '100%', height: 250, flex: 1},
  viewTitle: {flexDirection: 'row', justifyContent: 'space-between'},
  name: {
    marginTop: 15,
    fontSize: 24,
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
  },

  description: {
    marginTop: 10,
    fontSize: 15,
    fontFamily: Platform.OS === 'android' ? 'sans-serif' : 'Helvetica',
    textAlign: 'justify',
    marginBottom: 15,
  },
  viewIcon: {
    flexDirection: 'row',
  },
  textIcon: {
    marginLeft: 10,
    marginBottom: 8,
  },
  viewRow: {
    marginBottom: 15,
  },
  titleRow: {
    fontSize: 15,
  },
  subTitleRow: {
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    fontSize: 20,
  },
  label: {marginTop: 10},
  viewType: {
    backgroundColor: constants.primaryDarkColor,
    borderRadius: 10,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textType: {
    color: 'white',
  },
  textCity: {
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    textAlign: 'right',
    marginTop: 5,
  },
});
