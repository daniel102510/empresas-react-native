import {StyleSheet, Platform} from 'react-native';
import constants from '../../utils/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    flexDirection: 'column',
    paddingHorizontal: '5%',
  },
  viewLogo: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewInputs: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewInput: {
    width: '100%',
    marginBottom: 20,
  },
  textInput: {
    width: '100%',
    height: 47,
    backgroundColor: constants.primaryLightColor,
    color: 'black',
    borderRadius: 6,
    paddingLeft: 35,
    fontSize: 20,
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
  },
  icon: {
    position: 'absolute',
    top: 14,
    left: 10,
  },
  viewButtons: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLogin: {
    backgroundColor: constants.primaryDarkColor,
    width: '100%',
    height: 60,
    fontSize: 20,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLogin: {
    color: 'white',
    fontSize: 24,
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
  },
});
