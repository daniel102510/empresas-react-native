import React from 'react';
import {TextInput, View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login as loginAction} from '../../states/ducks/user';

// Components
import Loader from '../../components/Loader';

// Styles
import Styles from './styles';

const Login = (props) => {
  const {loading, actions} = props;

  const login = 'testeapple@ioasys.com.br';
  const password = '12341234';

  const image = require('../../assets/images/logo_ioasys.png');

  return (
    <>
      <View style={Styles.container}>
        <View style={Styles.viewLogo}>
          <Image source={image} />
        </View>
        <View style={Styles.viewInputs}>
          <View style={Styles.viewInput}>
            <TextInput
              style={Styles.textInput}
              value={login}
              editable={false}
            />
            <Icon style={Styles.icon} name="user" size={20} color="black" />
          </View>
          <View style={Styles.viewInput}>
            <TextInput
              style={Styles.textInput}
              value={password}
              editable={false}
              secureTextEntry
            />
            <Icon style={Styles.icon} name="lock" size={20} color="black" />
          </View>
        </View>
        <View style={Styles.viewButtons}>
          <TouchableOpacity
            disabled={_.get(loading, 'isLoading')}
            style={Styles.buttonLogin}
            activeOpacity={0.8}
            onPress={() => actions.login(login, password)}>
            <Text style={Styles.textLogin}>Entrar</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Loader />
    </>
  );
};

const mapStateToProps = (state) => {
  const {loadingReducer} = state;

  return {
    loading: loadingReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      login: bindActionCreators(loginAction, dispatch),
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
