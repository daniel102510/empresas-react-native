import {StyleSheet, Platform} from 'react-native';
import constants from '../../utils/constants';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    paddingHorizontal: '5%',
  },
  text: {
    color: constants.primaryDarkColor,
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    fontSize: 25,
  },
  textLegend: {
    color: 'rgba(0,0,0, 0.6)',
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    fontSize: 17,
    marginLeft: 4,
  },
  viewIconLogout: {justifyContent: 'center'},
  viewSearch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    paddingHorizontal: '5%',
  },
  inputSearch: {
    borderColor: constants.primaryLightColor,
    borderWidth: 1,
    borderRadius: 10,
    width: '80%',
    height: 50,
    paddingLeft: 10,
  },
  buttonSearch: {
    width: '17%',
    backgroundColor: constants.primaryDarkColor,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonClear: (active) => ({
    width: '17%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: active ? '#bd514d' : constants.primaryLightColor,
    borderWidth: 1,
  }),
  viewType: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 25,
    marginBottom: 10,
    marginHorizontal: '5%',
  },
  viewSelect: {
    width: '80%',
    borderColor: constants.primaryLightColor,
    borderWidth: 1,
    borderRadius: 10,
  },
  selectType: {
    height: 50,
    width: '100%',
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    color: 'rgba(0,0,0,0.4)',
  },
  scrollviewContent: {
    paddingTop: 20,
  },
});
