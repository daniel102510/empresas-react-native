import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {Picker} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';

// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {logout as logoutAction} from '../../states/ducks/user';
import {
  loadEnterprises as loadEnterprisesAction,
  clearEnterpraiseDetail as clearEnterpraiseDetailAction,
} from '../../states/ducks/enterprises';

// Components
import Loader from '../../components/Loader';
import EnterpriseItemList from '../../components/EnterpriseItemList';

// Styles
import Styles from './styles';
import constants from '../../utils/constants';

const Home = (props) => {
  const {user, enterprises, navigation, actions} = props;

  const [search, setSearch] = useState('');
  const [type, setType] = useState('');

  useEffect(() => {
    actions.loadEnterprises(type, search);
  }, []);

  return (
    <SafeAreaView>
      <View style={Styles.container}>
        <View style={Styles.viewHeader}>
          <View>
            <Text style={[Styles.text]}>
              {_.get(user, 'user.investor.investor_name')}
            </Text>
            <Text style={[Styles.textLegend]}>
              {_.get(user, 'user.investor.email')}
            </Text>
          </View>
          <TouchableOpacity
            style={Styles.viewIconLogout}
            activeOpacity={0.8}
            onPress={() => actions.logout()}>
            <Icon
              name="sign-out"
              size={30}
              color={constants.primaryDarkColor}
            />
          </TouchableOpacity>
        </View>

        <View style={Styles.viewSearch}>
          <TextInput
            placeholder="Filter by name"
            style={Styles.inputSearch}
            onChangeText={(text) => setSearch(text)}
            value={search}
          />
          <TouchableOpacity
            style={Styles.buttonSearch}
            activeOpacity={0.7}
            onPress={() => {
              actions.loadEnterprises(type, search);
            }}>
            <Icon name="search" size={20} color="white" />
          </TouchableOpacity>
        </View>

        <View style={Styles.viewType}>
          <View style={Styles.viewSelect}>
            <Picker
              selectedValue={type}
              style={Styles.selectType}
              mode="dialog"
              onValueChange={async (itemValue, itemIndex) => {
                await setType(itemValue);
                actions.loadEnterprises(itemValue, search);
              }}>
              <Picker.Item label="Filter by type" value="" />
              {_.get(enterprises, 'listTypes.length') > 0 &&
                _.get(enterprises, 'listTypes').map((item) => (
                  <Picker.Item
                    key={item.id}
                    label={item.enterprise_type_name}
                    value={item.id}
                  />
                ))}
            </Picker>
          </View>

          <TouchableOpacity
            style={Styles.buttonClear(type !== '' || search !== '')}
            activeOpacity={0.7}
            onPress={() => {
              if (type !== '' || search !== '') {
                setType('');
                setSearch('');
                actions.loadEnterprises('', '');
              }
            }}>
            <Icon
              name="trash"
              size={20}
              color={
                type !== '' || search !== ''
                  ? '#bd514d'
                  : constants.primaryLightColor
              }
            />
          </TouchableOpacity>
        </View>

        <ScrollView styles={Styles.scrollview}>
          <View style={Styles.scrollviewContent}>
            {_.get(enterprises, 'listEnterprises.length') > 0 &&
              _.get(enterprises, 'listEnterprises').map((item) => (
                <EnterpriseItemList
                  key={item.id}
                  enterprise={item}
                  onPress={async () => {
                    actions.clearEnterpraiseDetail();
                    setTimeout(() => {
                      navigation.navigate('Enterprise', {id: item.id});
                    }, 200);
                  }}
                />
              ))}
          </View>
        </ScrollView>
      </View>
      <Loader />
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  const {loadingReducer, userReducer, enterprisesReducer} = state;

  return {
    loading: loadingReducer,
    user: userReducer,
    enterprises: enterprisesReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      logout: bindActionCreators(logoutAction, dispatch),
      loadEnterprises: bindActionCreators(loadEnterprisesAction, dispatch),
      clearEnterpraiseDetail: bindActionCreators(
        clearEnterpraiseDetailAction,
        dispatch,
      ),
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
