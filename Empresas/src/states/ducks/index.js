import {combineReducers} from 'redux';
import loadingReducer from './loading';
import userReducer from './user';
import enterprisesReducer from './enterprises';

const allReducers = combineReducers({
  loadingReducer,
  userReducer,
  enterprisesReducer,
});

export default allReducers;
