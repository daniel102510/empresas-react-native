// Action Types

export const Types = {
  SHOW_LOADING: 'SHOW_LOADING',
  HIDE_LOADING: 'HIDE_LOADING',
};

// Reducer

const initialState = {
  isLoading: false,
  mensagem: '',
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case Types.SHOW_LOADING:
      return {
        isLoading: true,
        mensagem: action.mensagem,
      };
    case Types.HIDE_LOADING:
      return {
        isLoading: false,
        mensagem: '',
      };
    default:
      return state;
  }
}

// Action Creators

export function showLoading(mensagem) {
  return {
    type: Types.SHOW_LOADING,
    mensagem,
  };
}

export function hideLoading() {
  return {
    type: Types.HIDE_LOADING,
  };
}
