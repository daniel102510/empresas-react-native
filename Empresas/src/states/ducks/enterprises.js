// Action Types

export const Types = {
  LOAD: 'LOAD',
  LOAD_SUCCESS: 'LOAD_SUCCESS',
  LOAD_TYPES: 'LOAD_TYPES',
  LOAD_TYPES_SUCCESS: 'LOAD_TYPES_SUCCESS',
  FETCH_ENTERPRAISE: 'FETCH_ENTERPRAISE',
  FETCH_ENTERPRAISE_SUCCESS: 'FETCH_ENTERPRAISE_SUCCESS',
  CLEAR_ENTERPRAISE_DETAIL: 'CLEAR_ENTERPRAISE_DETAIL',
};

// Reducer

const initialState = {
  listEnterprises: [],
  listTypes: null,
  enterpraiseDetail: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case Types.LOAD_SUCCESS:
      return {
        ...state,
        ...{
          listEnterprises: action.data.enterprises,
        },
      };
    case Types.LOAD_TYPES_SUCCESS:
      return {
        ...state,
        ...{
          listTypes: action.data.types,
        },
      };
    case Types.FETCH_ENTERPRAISE_SUCCESS:
      return {
        ...state,
        ...{
          enterpraiseDetail: action.data.enterprise,
        },
      };
    case Types.CLEAR_ENTERPRAISE_DETAIL:
      return {
        ...state,
        ...{enterpraiseDetail: null},
      };
    default:
      return state;
  }
}

// Action Creators

export function loadEnterprises(type, name) {
  return {
    type: Types.LOAD,
    data: {type, name},
  };
}

export function loadEnterprisesSuccess(data) {
  return {
    type: Types.LOAD_SUCCESS,
    data,
  };
}

export function loadTypes() {
  return {
    type: Types.LOAD_TYPES,
  };
}

export function loadTypesSuccess(data) {
  return {
    type: Types.LOAD_TYPES_SUCCESS,
    data,
  };
}

export function fetchEnterprise(id) {
  return {
    type: Types.FETCH_ENTERPRAISE,
    data: {id},
  };
}

export function fetchEnterpriseSuccess(data) {
  return {
    type: Types.FETCH_ENTERPRAISE_SUCCESS,
    data,
  };
}

export function clearEnterpraiseDetail() {
  return {
    type: Types.CLEAR_ENTERPRAISE_DETAIL,
  };
}
