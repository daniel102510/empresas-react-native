// Action Types

export const Types = {
  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGOUT: 'LOGOUT',
};

// Reducer

const initialState = {
  isLogged: false,
  user: {},
  accessToken: '',
  uid: '',
  client: '',
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case Types.LOGIN_SUCCESS:
      return {
        isLogged: true,
        user: action.data.user,
        accessToken: action.data.accessToken,
        uid: action.data.uid,
        client: action.data.client,
      };
    case Types.LOGOUT:
      return {
        isLogged: false,
        user: {},
        accessToken: '',
        uid: '',
        client: '',
      };
    default:
      return state;
  }
}

// Action Creators

export function login(user, password) {
  return {
    type: Types.LOGIN,
    data: {user, password},
  };
}

export function loginSuccess(data) {
  return {
    type: Types.LOGIN_SUCCESS,
    data,
  };
}
export function logout() {
  return {
    type: Types.LOGOUT,
  };
}
