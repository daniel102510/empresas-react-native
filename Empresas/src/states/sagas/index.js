import {all} from 'redux-saga/effects';
import {watchLogin} from './user';
import {watchLoadEnterprises, watchFetchEnterprise} from './enterprises';

function* rootSaga() {
  yield all([watchLogin(), watchLoadEnterprises(), watchFetchEnterprise()]);
}

export default rootSaga;
