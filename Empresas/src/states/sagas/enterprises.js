import {takeLatest, select, call, put} from 'redux-saga/effects';
import {
  Types,
  loadEnterprisesSuccess,
  loadTypesSuccess,
  fetchEnterpriseSuccess,
} from '../ducks/enterprises';
import {showLoading, hideLoading} from '../ducks/loading';
import _ from 'lodash';

import Services from '../../utils/services';

export function* load(action) {
  yield put(showLoading('Fetch Enterprises'));

  const user = yield select((state) => state.userReducer);
  const {listTypes} = yield select((state) => state.enterprisesReducer);

  try {
    const response = yield call(
      Services.enterprises,
      user,
      action.data.type,
      action.data.name,
    );

    if (_.get(response, 'status') === 200) {
      const enterprises = _.get(response, 'data.enterprises');
      const types = enterprises
        .map((item) => item.enterprise_type)
        .filter(
          (elem, pos, self) =>
            !self[JSON.stringify(elem)] && (self[JSON.stringify(elem)] = true),
        );

      yield put(
        loadEnterprisesSuccess({
          enterprises,
        }),
      );

      if (listTypes === null) {
        yield put(
          loadTypesSuccess({
            types,
          }),
        );
      }
    }
  } catch (er) {
    console.log(er);
  } finally {
    yield put(hideLoading());
    return action;
  }
}

export function* fetch(action) {
  yield put(showLoading('Fetch Enterprise'));

  const user = yield select((state) => state.userReducer);

  try {
    const response = yield call(Services.enterprise, user, action.data.id);

    if (_.get(response, 'status') === 200) {
      const enterprise = _.get(response, 'data.enterprise');
      yield put(
        fetchEnterpriseSuccess({
          enterprise,
        }),
      );
    }
  } catch (er) {
    //
  } finally {
    yield put(hideLoading());
    return action;
  }
}

export function* watchLoadEnterprises() {
  yield takeLatest(Types.LOAD, load);
}

export function* watchFetchEnterprise() {
  yield takeLatest(Types.FETCH_ENTERPRAISE, fetch);
}
