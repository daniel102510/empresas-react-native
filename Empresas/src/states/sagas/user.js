import {takeLatest, call, put} from 'redux-saga/effects';
import {Types, loginSuccess} from '../ducks/user';
import {showLoading, hideLoading} from '../ducks/loading';
import _ from 'lodash';

import Services from '../../utils/services';

export function* login(action) {
  yield put(showLoading('Fetch Login'));
  try {
    const response = yield call(
      Services.login,
      action.data.user,
      action.data.password,
    );

    if (_.get(response, 'status') === 200) {
      const data = _.get(response, 'data');
      const headers = _.get(response, 'headers');

      const loginObject = {
        user: data,
        accessToken: _.get(headers, 'access-token'),
        client: _.get(headers, 'client'),
        uid: _.get(headers, 'uid'),
      };

      yield put(loginSuccess(loginObject));
    }
  } catch (er) {
    //
  } finally {
    yield put(hideLoading());
    return action;
  }
}

export function* watchLogin() {
  yield takeLatest(Types.LOGIN, login);
}
