import axios from 'axios';
import constants from './constants';
import _ from 'lodash';

const Api = {
  login: (email, password) =>
    new Promise((resolve, reject) => {
      axios({
        url: `${constants.apiEndpoint}users/auth/sign_in`,
        method: 'POST',
        data: {email, password},
      })
        .then((response) => resolve(response))
        .catch((e) => reject(e));
    }),
  enterprises: (userConfig, type, name) => {
    const headers = {
      'access-token': userConfig.accessToken,
      client: userConfig.client,
      uid: userConfig.uid,
    };

    let params = [];
    if (type !== '') {
      params.push(`enterprise_types=${type}`);
    }

    if (name !== '') {
      params.push(`name=${name}`);
    }

    return new Promise((resolve, reject) => {
      axios({
        url: `${constants.apiEndpoint}enterprises?${params.join('&')}`,
        method: 'GET',
        headers,
      })
        .then((response) => resolve(response))
        .catch((e) => reject(e));
    });
  },
  enterprise: (userConfig, id) =>
    new Promise((resolve, reject) => {
      const headers = {
        'access-token': userConfig.accessToken,
        client: userConfig.client,
        uid: userConfig.uid,
      };
      axios({
        url: `${constants.apiEndpoint}enterprises/${id}`,
        method: 'GET',
        headers,
      })
        .then((response) => resolve(response))
        .catch((e) => reject(e));
    }),
};

export default Api;
