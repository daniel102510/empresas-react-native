const constants = {
  // API
  endpoint: 'https://empresas.ioasys.com.br',
  apiEndpoint: 'https://empresas.ioasys.com.br/api/v1/',
  // APP
  loadingColor: 'white',
  primaryColor: '#e4e2f6',
  primaryLightColor: '#ecebf7',
  primaryDarkColor: '#8d84de',
  // primaryTextColor: '#968de5',
};

export default constants;
