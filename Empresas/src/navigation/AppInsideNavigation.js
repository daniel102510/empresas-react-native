import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';
import _ from 'lodash';

// Screens
import Login from '../screens/Login';
import Home from '../screens/Home';
import Enterprise from '../screens/Enterprise';

const Stack = createStackNavigator();

const AppInsideNavigation = (props) => {
  const {user} = props;

  if (_.get(user, 'isLogged')) {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Enterprise" component={Enterprise} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = (state) => {
  const {userReducer} = state;

  return {
    user: userReducer,
  };
};

export default connect(mapStateToProps)(AppInsideNavigation);
