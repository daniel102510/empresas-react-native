import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewLoader: {
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    width: 100,
    height: 100,
    borderRadius: 10,
  },
});
