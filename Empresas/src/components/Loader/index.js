import React from 'react';
import {View} from 'react-native';
import {Spinner} from 'native-base';
import _ from 'lodash';

// Redux
import {connect} from 'react-redux';

// Utils
import constants from '../../utils/constants';

// Styles
import Styles from './styles';

const Loader = (props) => {
  const {loading} = props;
  return (
    <>
      {_.get(loading, 'isLoading') && (
        <View style={Styles.container}>
          <View style={Styles.viewLoader}>
            <Spinner color={constants.loadingColor} />
          </View>
        </View>
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  const {loadingReducer} = state;

  return {
    loading: loadingReducer,
  };
};

export default connect(mapStateToProps)(Loader);
