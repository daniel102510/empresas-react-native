import {constant} from 'lodash';
import {StyleSheet, Platform} from 'react-native';
import constants from '../../utils/constants';

export default StyleSheet.create({
  card: {
    marginHorizontal: '5%',
    paddingHorizontal: 10,
    marginBottom: 20,
    backgroundColor: constants.primaryLightColor,
    flexDirection: 'row',
    borderRadius: 5,
    paddingVertical: 10,
  },
  image: {width: 50, height: 50, borderRadius: 5, flex: 1},
  info: {
    marginLeft: 15,
    flex: 5,
  },
  textName: {
    fontWeight: 'bold',
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
  },
  textCity: {
    color: 'rgba(0,0,0,0.5)',
    fontFamily: Platform.OS === 'android' ? 'sans-serif' : 'Helvetica',
  },
  textDescription: {
    marginRight: 5,
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
  },
  label: {justifyContent: 'flex-end'},
  viewType: {
    borderRadius: 10,
    backgroundColor: constants.primaryDarkColor,
    paddingHorizontal: 7,
  },
  textType: {
    color: 'white',
  },
});
