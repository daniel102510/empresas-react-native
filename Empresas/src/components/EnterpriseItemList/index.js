import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import _ from 'lodash';

// Utils
import constants from '../../utils/constants';

// Styles
import Styles from './styles';

const EnterpriseItemList = (props) => {
  const {enterprise, onPress} = props;

  return (
    <TouchableOpacity
      style={Styles.card}
      key={_.get(enterprise, 'id')}
      activeOpacity={0.7}
      onPress={() => onPress()}>
      <Image
        style={Styles.image}
        source={{uri: `${constants.endpoint}${_.get(enterprise, 'photo')}`}}
      />
      <View style={Styles.info}>
        <Text style={Styles.textName}>
          {_.get(enterprise, 'enterprise_name')}
        </Text>
        <Text style={Styles.textCity}>{`${_.get(enterprise, 'city')} / ${_.get(
          enterprise,
          'country',
        )}`}</Text>
        <Text style={Styles.textDescription} numberOfLines={1}>
          {_.get(enterprise, 'description')}
        </Text>
      </View>
      <View style={Styles.label}>
        <View style={Styles.viewType}>
          <Text style={Styles.textType}>
            {_.get(enterprise, 'enterprise_type.enterprise_type_name')}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default EnterpriseItemList;
