import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

// Redux
import {connect} from 'react-redux';

// Styles
import Styles from './styles';

const Loader = (props) => {
  const {title, onPress} = props;
  return (
    <View style={Styles.card}>
      <Text style={Styles.title}>{title}</Text>
      <TouchableOpacity
        style={Styles.viewIcon}
        activeOpacity={0.8}
        onPress={() => onPress()}>
        <Icon name="chevron-left" size={20} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = (state) => {
  const {loadingReducer} = state;

  return {
    loading: loadingReducer,
  };
};

export default connect(mapStateToProps)(Loader);
