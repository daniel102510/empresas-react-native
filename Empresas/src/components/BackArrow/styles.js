import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
  card: {
    width: '100%',
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 20,
    backgroundColor: 'white',
  },
  title: {
    fontFamily:
      Platform.OS === 'android' ? 'sans-serif-thin' : 'Helvetica-Light',
    fontSize: 20,
  },
  viewIcon: {
    position: 'absolute',
    top: 28,
    left: 20,
  },
});
