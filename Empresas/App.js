import React from 'react';
import {StatusBar, LogBox} from 'react-native';
import {Provider} from 'react-redux';

// Redux
import store from './src/states';

// Navigation
import AppInsideNavigation from './src/navigation/AppInsideNavigation';

LogBox.ignoreLogs(['Picker']); // Ignore log notification by message

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />

      <AppInsideNavigation />
    </Provider>
  );
};

export default App;
